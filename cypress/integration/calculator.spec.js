describe('My first test', function() {
  it('1 + 2 = 3', function() {
    cy.visit('/')
    cy.get('#left').type('1')
    cy.get('#right').type('2')
    cy.get('#result').should('have.text', '3')
    cy.percySnapshot()
  })
})